package com.svlugovoy.clappbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClAppBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClAppBeApplication.class, args);
    }

}
