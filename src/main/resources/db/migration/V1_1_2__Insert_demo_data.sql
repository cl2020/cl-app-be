INSERT INTO
  contacts (first_name, last_name, email, phone, gender, dob, city, country)
VALUES
  ('Ivan', 'Ivanov', 'ivan@gmail.com', '+380975050511', 'MALE', '1990-11-25', 'Kyiv', 'Ukraine'),
  ('Nataly', 'Orlova', 'natorl@gmail.com', '+380975050512', 'FEMALE', '1985-01-10', 'Poltava', 'Ukraine'),
  ('Ben', 'Sanches', 'sanches@gmail.com', '+380975050513', 'MALE', '1980-11-15', 'Warsaw', 'Poland'),
  ('Anna', 'Antonova', 'ant.ann@gmail.com', '+380975054413', 'FEMALE', '1980-11-15', 'Boston', 'United States'),
  ('Robert', 'Martin', 'bob@gmail.com', '+380975050513', 'MALE', '1975-05-22', 'Dallas', 'United States'),
  ('Masha', 'Mashina', 'mashulka@gmail.com', '+380778880517', 'FEMALE', '1970-11-15', 'Barcelona', 'Spain'),
  ('Jack', 'Rock', 'rock777@gmail.com', '+380975081113', 'MALE', '1980-11-15', 'London', 'England'),
  ('Uriy', 'Uriev', 'uriev.u@gmail.com', '+380936450517', 'MALE', '1980-11-15', 'Milan', 'Italy'),
  ('John', 'Smith', 'johny@gmail.com', '+380973350516', 'MALE', '1980-11-15', 'Los Angles', 'United States'),
  ('Olga', 'Olgina', 'olga@gmail.com', '+380974560513', 'FEMALE', '1980-11-15', 'Paris', 'France'),
  ('Petr', 'Petrov', 'petro@gmail.com', '+380975050533', 'MALE', '1980-11-15', 'New York', 'United States');

